package algo.core;

/**
 * Created by rohit on 3/24/17.
 */
public class CoinChange {

    public static long makeChange(int[] coins, int money, int index) {
        if (coins.length == index)
            return 0;
        if (coins[index] == money)
            return coins[index];
        else return makeChange(coins, money, ++index)
                + makeChange(coins, money - coins[index], ++index);
    }


    // 6 5,1 ->
    public static void main(String[] args) {
        int[] coins = new int[]{1, 5, 10, 25};
        System.out.println(makeChange(coins, 27, 0));
    }

}
