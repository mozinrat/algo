package algo.core;

/**
 * Created by rohit on 3/24/17.
 */
public class Trie {
    private static final int NUMBER_OF_CHARS = 26;
    private Trie[] children = new Trie[NUMBER_OF_CHARS];

    private static int getCharIndex(char c) {
        return c - 'a';
    }

    private Trie getTrie(char c) {
        return children[getCharIndex(c)];
    }

    private void setTrie(char c, Trie t) {
        children[getCharIndex(c)] = t;
    }

    public void add(String s) {
        add(s, 0);
    }

    private void add(String s, int index) {
        if (s.length() == index) return;
        char current = s.charAt(index);
        Trie child = getTrie(current);
        if (child == null) {
            child = new Trie();
        }
        setTrie(current, child);
        child.add(s, ++index);
    }

}
