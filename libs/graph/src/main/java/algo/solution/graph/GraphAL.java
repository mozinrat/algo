package algo.solution.graph;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rohit on 3/24/17.
 */
public class GraphAL<V> {
    private static final int INF = Integer.MAX_VALUE;

    private int V;
    public Map<Vertex<V>,Vertex<V>> adjacencyList;
    public GraphAL(int v) {
        V = v;
        adjacencyList = new HashMap<>(v*(v-1));
    }

    public void addEdge(Vertex<V> u, Vertex<V> v, int weight) {
        adjacencyList.put(u,v);
    }

    public void addEdge(V u,V v, int weight) {
        addEdge(new Vertex<V>(u),new Vertex<V>(v),weight);
    }
}

class Vertex<V> {
    private V data;
    private Vertex<V> next;

    public Vertex(V data) {
        this.data = data;
    }
    public V getData() {
        return data;
    }

    public Vertex<V> getNext() {
        return next;
    }

    public void setNext(Vertex<V> next) {
        this.next = next;
    }
}
