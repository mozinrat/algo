package algo.solution.graph;

/**
 * Created by rohit on 3/24/17.
 */
public class Solution4 {
    public static void main(String[] args) {
        GraphAL<Integer> g = new GraphAL<>(4);
        g.addEdge(1,2,1);
        g.addEdge(1,3,1);
        g.addEdge(1,4,1);
        g.addEdge(2,1,1);
        g.addEdge(2,3,1);
        g.addEdge(2,4,1);
        g.addEdge(4,2,1);
        System.out.println(g);
    }
}
