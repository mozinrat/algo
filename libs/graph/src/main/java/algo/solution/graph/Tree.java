package algo.solution.graph;

/**
 * Created by rohit on 3/27/17.
 */
public class Tree<V> {
    public V data;
    private Tree<V> left;
    private Tree<V> right;

    public Tree(V data) {
        this.data = data;
    }

    public Tree<V> getLeft() {
        return left;
    }

    public Tree<V> getRight() {
        return right;
    }

    public void addLeft(V data){
        left = new Tree<V>(data);
    }

    public void addRigt(V data){
        right = new Tree<V>(data);
    }

}
