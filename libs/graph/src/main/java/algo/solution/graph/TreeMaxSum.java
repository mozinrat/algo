package algo.solution.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by rohit on 3/27/17.
 */
public class TreeMaxSum {

    public static void main(String[] args) {
        Tree<Integer> binaryTree = new Tree<>(1);
        binaryTree.addLeft(2);
        binaryTree.addRigt(3);

        binaryTree.getLeft().addLeft(4);
        binaryTree.getLeft().addRigt(5);

        binaryTree.getRight().addLeft(6);
        binaryTree.getRight().addRigt(7);

        binaryTree.getLeft().getLeft().addLeft(3);

        binaryTree.getLeft().getRight().addLeft(10);

        binaryTree.getRight().getLeft().addRigt(8);

        System.out.println(maxSumDfs(binaryTree, new Result<>(0, new ArrayList<>())).path);
    }

    private static void printBFS(Tree<Integer> binaryTree) {
        Queue<Tree<Integer>> queue = new LinkedList<>();
        queue.add(binaryTree);
        printBFS(queue);
    }

    private static void printBFS(Queue<Tree<Integer>> queue) {
        while (!queue.isEmpty()) {
            Tree<Integer> node = queue.poll();
            System.out.println(node.data);
            if (node.getLeft() != null) {
                queue.add(node.getLeft());
            }
            if (node.getRight() != null) {
                queue.add(node.getRight());
            }
        }
    }

    private static void printDFS(Tree<Integer> node) {
        System.out.println(node.data);
        if (node.getLeft() != null) {
            printDFS(node.getLeft());
        }
        if (node.getRight() != null) {
            printDFS(node.getRight());
        }
    }

    private static Result<Integer> maxSumDfs(Tree<Integer> node, Result<Integer> result) {
        if (node.getLeft() == null && node.getRight() == null) {
            result.maxSum = result.maxSum + node.data;
            result.path.add(node.data);
        } else if (node.getLeft() != null && node.getRight() == null) {
            Result<Integer> sumLeft = maxSumDfs(node.getLeft(), new Result<>(result.maxSum + node.data, result.path));
            if (sumLeft.maxSum > result.maxSum) {
                result.path.add(node.getLeft().data);
            }
        } else if (node.getLeft() == null && node.getRight() != null) {
            Result<Integer> sumRight = maxSumDfs(node.getRight(), new Result<>(result.maxSum + node.data, result.path));
            if (sumRight.maxSum > result.maxSum) {
                result.path.add(node.getRight().data);
            }
        } else {
            Result<Integer> sumLeft = maxSumDfs(node.getLeft(), new Result<>(result.maxSum + node.data, result.path));
            Result<Integer> sumRight = maxSumDfs(node.getRight(), new Result<>(result.maxSum + node.data, result.path));
            if (sumLeft.maxSum > sumRight.maxSum && sumLeft.maxSum > result.maxSum) {
                result.path.add(node.getLeft().data);
            } else if (sumLeft.maxSum <= sumRight.maxSum && sumRight.maxSum > result.maxSum) {
                result.path.add(node.getRight().data);
            }
        }
        return result;
    }

    static class Result<V> {
        int maxSum;
        List<V> path;

        public Result(int maxSum, List<V> path) {
            this.maxSum = maxSum;
            this.path = path;
        }
    }
}






