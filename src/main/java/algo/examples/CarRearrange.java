package algo.examples;


import java.util.HashMap;

/**
 * Created by rohit on 3/24/17.
 */
public class CarRearrange {
    public static void main(String[] args) {
        String defaultPos = "ABCDEFG_";
        String now = "A_DEBFGC";
        HashMap<Character,Integer> nowPosMap = new HashMap<>();
        int posCursor = 0;
        for (int j = 0; j < now.length(); j++) {
            char c = now.charAt(j);
            if (c == '_') {
                posCursor = j;
            } else if (defaultPos.charAt(j) != c) {
                nowPosMap.put(c,j);
            }
        }
        // do the shuffling on now string
        char[] nowArray = now.toCharArray();
        for(int i=0;i<nowPosMap.size();i++){
            Character errChar = defaultPos.charAt(posCursor);
            nowArray[posCursor] = errChar.charValue();
            posCursor = nowPosMap.get(errChar);
            nowArray[posCursor]='_';
        }

        System.out.println(new String(nowArray));
    }
}
