package algo.examples;

/**
 * Created by rohit on 2/5/17.
 */
public class DisjointSet {

    void initialize(int Arr[], int size[],int N) {
        for (int i = 0; i < N; i++) {
            Arr[i] = i;
            size[i] = 1;
        }
    }

    void weightedUnion(int Arr[], int size[], int A, int B) {
        int root_A = root(Arr,A);
        int root_B = root(Arr,B);
        if (size[root_A] < size[root_B]) {
            Arr[root_A] = Arr[root_B];
            size[root_B] += size[root_A];
        } else {
            Arr[root_B] = Arr[root_A];
            size[root_A] += size[root_B];
        }

    }

    int root (int Arr[] ,int i)
    {
        while(Arr[ i ] != i) {
            Arr[ i ] = Arr[ Arr[ i ] ] ;
            i = Arr[ i ];
        }
        return i;
    }

    public static void main(String[] args) {

    }
}
