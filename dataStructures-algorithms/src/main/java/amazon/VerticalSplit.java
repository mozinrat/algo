package amazon;


import tree.TreeNode;

import java.util.HashMap;

/**
 * Created by rohit on 6/3/17.
 */
public class VerticalSplit {

    public static void main(String[] args) {
        TreeNode node = new TreeNode(1);
        node.left = new TreeNode(2);

        node.left.left = new TreeNode(4);
        node.left.right = new TreeNode(5);

        node.right = new TreeNode(3);
        node.right.left = new TreeNode(6);
        node.right.right = new TreeNode(7);

        HashMap<Integer, Integer> hm = new HashMap<>();
        printVerticalSum(node, 0, hm);
        System.out.println(hm);
    }

    private static void printVerticalSum(TreeNode node, int depth, HashMap<Integer, Integer> hm) {
        if (node == null) return;
        put(hm, depth, node.val);
        printVerticalSum(node.left, depth - 1, hm);
        printVerticalSum(node.right, depth + 1, hm);
    }

    private static void put(HashMap<Integer, Integer> hm, int key, int val) {
        if (hm.containsKey(key)) {
            int temp = hm.get(key) + val;
            hm.put(key, temp);
        } else {
            hm.put(key, val);
        }
    }
}
