package amazon;

/**
 * Created by rohit on 6/3/17.
 */
public class Spiral {
    public static void main(String[] args) {
        int[][] m = {
                {1, 2, 3, 4, 5, 6},
                {7, 8, 9, 10, 11, 12},
                {13, 14, 15, 16, 17, 18},
                {19, 20, 21, 22, 23, 24},
                {25, 26, 27, 28, 29, 30},
                {31, 32, 33, 34, 35, 36}
        };

        printSpiral(m, m.length - 1, m[0].length - 1, 0, 0);

    }

    static void printSpiral(int[][] m, int endX, int endY, int startX, int startY) {
        for (int i = startY; i < endY; i++) {
            System.out.print(m[startX][i] + " ,");
        }
        for (int i = startX; i < endX; i++) {
            System.out.print(m[i][endY] + " ,");
        }
        for (int j = endY; j > startY; j--) {
            System.out.print(m[endX][j] + " ,");
        }
        for (int k = endX; k > startX; k--) {
            System.out.print(m[k][startY] + " ,");
        }
        startX++;
        startY++;
        endX--;
        endY--;

        if (startX < endX && startY < endY)
            printSpiral(m, endX, endY, startX, startY);
        else return;
    }
}
