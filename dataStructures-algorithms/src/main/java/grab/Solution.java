package grab;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class Solution {
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm");

    public static void main(String[] args) {
        System.out.println(solution("11:45","15:15"));
    }

    public static int solution(String E, String L) {
        LocalDateTime enterance= LocalDateTime.parse("02/26/2017 "+E, formatter);
        LocalDateTime exit= LocalDateTime.parse("02/26/2017 "+L, formatter);
        int l = (int) Duration.between(enterance, exit).toMinutes();
        int totalHours =   (l%60 == 0) ? l/60 : (l/60)+1;
        int sum = 2;
        if(totalHours>0){
            sum+=3;
            totalHours--;
        }
        sum = sum+(totalHours*4);
        return sum;
    }

}

