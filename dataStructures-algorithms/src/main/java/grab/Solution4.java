package grab;

import java.util.Arrays;

public class Solution4 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(new Solution4().solution(new int[]{9, 1, 4, 9, 0, 4, 8, 9, 0, 1})));
    }

    private int[] solution(int[] t) {
        int[] dp = new int[t.length];
        int center = getCenter(t);
        for (int i = 0; i < t.length; i++) {
            if (i != center) dp(i, center, t, dp);
        }
        int[] ans = new int[t.length - 1];
        for (int i = 0; i < dp.length; i++) {
            if (i != center) ans[dp[i] - 1]++;
        }
        return ans;
    }

    private int getCenter(int[] t) {
        for (int i = 0; i < t.length; i++) {
            if (i == t[i]) return i;
        }
        throw new UnsupportedOperationException("No center found");
    }

    private int dp(int current, int center, int[] t, int[] dp) {
        if (dp[current] == 0) {
            int parent = t[current];
            if (parent == center) {
                dp[current] = 1;
                return 1;
            }
            dp[current] += dp(parent, center, t, dp) + 1;
        }
        return dp[current];
    }
}
