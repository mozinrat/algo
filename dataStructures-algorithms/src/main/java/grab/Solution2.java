package grab;

/**
 * Created by rohit on 5/24/17.
 */
public class Solution2 {
    public int solution(int[] A) {

        if (A.length < 4) {
            return getCostAllOneDay(A);
        } else if (A.length >= 23) {
            return getFullMonthPass();
        }

        int way1 = calculateWay(A, 1, 0) + 2;
        int way2 = calculateWay(A, 1, A[0] + 6) + 7;
        int way3 = 25;
        return getMinimum(way1, way2, way3);
    }


    private int getMinimum(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
    }

    private static int calculateWay(int[] A, int index, int max) {
        if (index >= A.length ) {
            return 0;
        } else if (A[index] <= max) {
            return calculateWay(A, index + 1, max);
        } else {
            return (Math.min(calculateWay(A, index + 1, A[index] + 6) + 7, calculateWay(A, index + 1, 0) + 2));
        }
    }

    private int getCostAllOneDay(int[] A) {
        return A.length * 2;
    }

    private int getFullMonthPass() {
        return 25;
    }

    public static void main(String[] args) {
        System.out.println(new Solution2().solution(new int[]{1, 4, 5, 7, 8, 29, 30}));
    }
}
