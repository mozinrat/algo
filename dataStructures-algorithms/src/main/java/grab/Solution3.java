package grab;

import java.util.*;

public class Solution3 {
    public HashMap<Integer, GraphNode> map = new HashMap<>();
    public GraphNode capital;

    public int[] solution(int[] input) {
        makeGraph(input);
        List<Integer> solution = new ArrayList<>();
        Queue<GraphNode> queue = new LinkedList<>();
        queue.add(capital);

        Queue<GraphNode> tempQueue = new LinkedList<>();

        while (true) {
            if (queue.isEmpty()) {
                break;
            }
            while (!queue.isEmpty()) {
                final GraphNode polled = queue.poll();
                tempQueue.addAll(polled.getChild());
            }
            solution.add(tempQueue.size());
            queue = new LinkedList<>(tempQueue);
            tempQueue.clear();
        }
        int[] sol = new int[input.length - 1];
        for (int i = 0; i < solution.size(); i++) {
            sol[i] = solution.get(i);
        }
        return sol;
    }

    public void makeGraph(int[] input) {
        for (int i = 0; i < input.length; i++) {
            GraphNode parent;
            GraphNode child;
            if (map.containsKey(i)) {
                parent = map.get(i);
            } else {
                parent = new GraphNode(i);
                map.put(i, parent);
            }

            if (map.containsKey(input[i])) {
                child = map.get(input[i]);
            } else {
                child = new GraphNode(input[i]);
                map.put(input[i], child);
            }


            if (parent.equals(child)) {
                capital = parent;
            } else {
                child.getChild().add(parent);
            }
        }
    }

    static class GraphNode {
        private int id;
        private List<GraphNode> child = new ArrayList<>();

        public GraphNode(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<GraphNode> getChild() {
            return child;
        }

        public void setChild(List<GraphNode> child) {
            this.child = child;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GraphNode graphNode = (GraphNode) o;
            return id == graphNode.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new Solution3().solution(new int[]{9, 1, 4, 9, 0, 4, 8, 9, 0, 1})));
    }
}