package code.ninja;

import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Solution s = new Solution();
        List<Integer> a= Arrays.asList(2,2,2,2,2);
        List<List<Integer>> b = Arrays.asList(
                Arrays.asList(4,1),
                Arrays.asList(3,1),
                Arrays.asList(3,1),
                Arrays.asList(2,1),
                Arrays.asList(4,2)
        );
        System.out.println(s.getMode(a,b));
    }

    public ArrayList<Integer> getMode(List<Integer> a, List<List<Integer>> b) {
        ArrayList<Integer> result = new ArrayList<>(b.size());
        HashMap<Integer,Integer> fm = new HashMap<>();
        for(int i:a){
            if(fm.containsKey(i)){
                int temp = fm.get(i)+1;
                fm.put(i,temp);
            } else {
                fm.put(i, 1);
            }
        }

        for (List<Integer> list : b) {
            int oval = a.get(list.get(0)-1);
            int prevFrequency = fm.get(oval);
            fm.put(oval,prevFrequency-1);

            int newFrequency = fm.get(list.get(1))!=null?fm.get(list.get(1))+1:1;
            fm.put(list.get(1),newFrequency);

            a.set(list.get(0)-1,list.get(1));
            int currentM =result.size()==0?Integer.MAX_VALUE:result.get(result.size()-1);
            int m = getModeFM(fm,currentM,list.get(1));
            result.add(m);
        }
        return result;
    }

    private int getModeFM(HashMap<Integer, Integer> fm,int current,int newV) {
        if(fm.get(current)==null) {
            int mode = Integer.MAX_VALUE, freq = 0;
            for (Map.Entry<Integer, Integer> en : fm.entrySet()) {
                if (en.getValue() > freq) {
                    freq = en.getValue();
                    mode = en.getKey();
                }
            }
            return mode;
        }else {
            if(fm.get(newV) > fm.get(current)){
                return newV;
            }else if(fm.get(newV) == fm.get(current)){
                return Math.min(newV,current);
            }else return current;
        }
    }

    public int cntMatrix(TreeNode a, TreeNode b) {
        int count = 0;
        if (a == null && b == null) return count;
        if (a == null && b != null) return count + getChildCount(b);
        if ((b.left == null && a.left != null) || (b.right == null && a.right != null)) return -1;

        int lc = cntMatrix(a.left, b.left);
        if (lc != -1) {
            count += lc;
        } else return -1;

        int rc = cntMatrix(a.right, b.right);
        if (rc != -1) {
            count += rc;
        } else return -1;
        return count;
    }

    int getChildCount(TreeNode a) {
        int count = 0;
        if (a == null) return count;
        count++;
        count += getChildCount(a.left);
        count += getChildCount(a.right);
        return count;
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}
