package code.ninja;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by rohit on 6/8/17.
 */
public class Solution2 {
    static int[] tubes = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};

    /**
     * 0 requires -- 6 tubes
     * 1 requires -- 2 tubes
     * 2 requires -- 5 tubes
     * 3 requires -- 5 tubes
     * 4 requires -- 4 tubes
     * 5 requires -- 5 tubes
     * 6 requires -- 6 tubes
     * 7 requires -- 3 tubes
     * 8 requires -- 7 tubes
     * 9 requires -- 6 tubes
     */
    public static void main(String[] args) {
        int cost = 100;
        int digits = 3;
//        List<Integer> collect = IntStream.range(0, (int) Math.pow(10, digits) - 1).filter(i -> noRepeatingDigit(i, cost)).boxed().collect(Collectors.toList());
//        System.out.println(collect.size());
        new Solution2().maxset(new ArrayList<>(Arrays.asList(1, 2, 5, -7, 2, 3)));
    }

    public ArrayList<Integer> maxset(ArrayList<Integer> a) {
        ArrayList<Integer> index = new ArrayList<>();
        if (a.get(0) > 0) index.add(0);

        for (int i = 1; i < a.size(); i++) {
            if (a.get(i) < 0) index.add(i - 1);
            if (a.get(i - 1) < 0) index.add(i + 1);
        }
        return index;
    }

    private static boolean noRepeatingDigit(int i, int cost) {
        Set<Integer> vals = new HashSet<>();
        if (i == 0) {
            vals.add(0);
        } else {
            while (i > 0) {
                if (vals.contains(i % 10)) {
                    return false;
                }
                vals.add(i % 10);
                i = i / 10;
            }
        }
        return isValid(vals, cost);
    }

    static boolean isValid(Set<Integer> digits, int cost) {
        for (Integer d : digits) {
            cost -= tubes[d];
        }
        return cost >= 0;
    }
}
