package skyscanners;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by rohit on 5/25/17.
 */
public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int N = Integer.parseInt(br.readLine());

        String employee1 = new String(br.readLine());
        String employee2 = new String(br.readLine());

        Map<String, String> managerEmpMap = new HashMap<>(N - 1);
        while (N > 1) {
            String[] temp2 = br.readLine().split(" ");
            managerEmpMap.put(temp2[1], temp2[0]);
            N--;
        }

        System.out.println(getLCA(managerEmpMap, employee1, employee2));

    }

    private static String getLCA(Map<String, String> hm, String emp1, String emp2) {
        HashSet<String> visited = new HashSet<>(hm.size());
        while (true) {
            if (visited.contains(emp1)) {
                return emp1;
            } else {
                visited.add(emp1);
            }
            if (visited.contains(emp2)) {
                return emp2;
            } else {
                visited.add(emp2);
            }

            emp1 = hm.get(emp1);
            emp2 = hm.get(emp2);
        }
    }
}

