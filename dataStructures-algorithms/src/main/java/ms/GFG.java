package ms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rohit on 5/26/17.
 */
public class GFG {
    static int[] x = new int[]{1, 1, 0, -1, -1, -1, 0, 1};
    static int[] y = new int[]{0, 1, 1, 1, 0, -1, -1, -1};

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine());
        List<IN> allIN = new ArrayList<>(T);
        while (T > 0) {
            String[] temp = br.readLine().split(" ");
            int rows = Integer.parseInt(temp[0]);
            int cols = Integer.parseInt(temp[1]);
            char[][] in = new char[rows][cols];
            String[] data = br.readLine().split(" ");
            for (int i = 0; i < rows; i++) {
                for (int k = 0; k < cols; k++) {
                    in[i][k] = data[i * cols + k].charAt(0);
                }
            }
            String q = br.readLine();
            allIN.add(new IN(in, q));
            T--;
        }
        for (IN a : allIN) {
            printWords(a.A, a.q);
            System.out.println();
        }

    }

    private static void printWords(char[][] boggle, String s) {
        for (int i = 0; i < boggle.length; i++) {
            for (int j = 0; j < boggle[0].length; j++) {
                boolean[][] visited = new boolean[boggle.length][boggle[0].length];
                dfs(boggle, i, j, "", visited, s);
            }
        }
    }

    private static void dfs(char[][] boggle, int i, int j, String s, boolean[][] visited, String q) {
        for (int ii = 0; ii < 8; ii++) {
            if (dfs(boggle, i, j, s, visited, q, ii))
                System.out.print(i + " " + j +",");
        }
    }

    private static boolean dfs(char[][] boggle, int i, int j, String builder, boolean[][] visited, String q, int direction) {
        int ii = i + x[direction], jj = j + y[direction];
        boolean f = false;
        if (ii >= 0 && jj >= 0) {
            String temp = builder + boggle[i][j];
            visited[i][j] = true;
            if (q.length() > temp.length() && ii < boggle.length && jj < boggle[0].length && !visited[ii][jj]) {
                f = dfs(boggle, ii, jj, temp, visited, q, direction);
            } else if (q.equals(temp)) {
                visited[i][j] = false;
                return true;
            }
        }
        visited[i][j] = false;
        return f;
    }

    static class IN {
        char[][] A;
        String q;

        public IN(char[][] a, String q) {
            A = a;
            this.q = q;
        }
    }
}
