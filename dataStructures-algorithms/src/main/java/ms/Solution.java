package ms;

/**
 * Created by rohit on 5/31/17.
 */
public class Solution {
    public static void main(String[] args) {
        printAllUniqueParts(5);
    }

    static void printAllUniqueParts(int n) {
        int partition[] = new int[n];
        int cursor = 0;
        partition[cursor] = n;

        // This loop first prints current partition, then generates next
        // partition. The loop stops when the current partition has all 1s
        while (true) {
            printArray(partition, cursor + 1);
            // Generate next partition

            int cpv = 0;
            while (cursor >= 0 && partition[cursor] == 1) {
                cpv += partition[cursor];
                cursor--;
            }

            // if cursor < 0, all the values are 1 so there are no more partitions
            // return will break the while loop
            if (cursor < 0) return;

            // Decrease the partition[cursor] found above and adjust the cpv
            partition[cursor]--;
            cpv++;

            while (cpv > partition[cursor]) {
                partition[cursor + 1] = partition[cursor];
                cpv = cpv - partition[cursor];
                cursor++;
            }

            partition[cursor + 1] = cpv;
            cursor++;
        }
    }

    static void printArray(int p[], int n) {
        for (int i = 0; i < n; i++)
            System.out.print(p[i] + " ");
        System.out.println();
    }
}
