package ms;

/** Created by rohit on 5/22/17. */
public class Playlist {

  public static void main(String[] args) {
    String[] songs = {"aa", "bb", "cc", "dd", "ab", "sd"};
    String q = "sd";
    int k = 0;
    System.out.println(getTurns(songs, q, k));
  }

  private static int getTurns(String[] songs, String q, int k) {
    int turn = 1;
    for (int i = k, j = k; i != j; turn++) {
      if (songs[i].equals(q) || songs[j].equals(q)) {
        return turn;
      }
      i = (++i % (songs.length - 1));
      j = (songs.length - 1 + --j) % (songs.length - 1);
    }
    return turn;
  }
}
