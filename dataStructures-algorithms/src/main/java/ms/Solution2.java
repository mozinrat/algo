package ms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by rohit on 5/31/17.
 */
public class Solution2 {
    public static void main(String[] args) {
        System.out.println(vs(new String[]{"A","A","B","B","C","C"}));
    }

    static String vs(String[] in) {
        int max = 0;
        HashMap<String, Integer> hm = new HashMap<>();
        for (String s : in) {
            if (hm.containsKey(s)) {
                int temp = hm.get(s) + 1;
                if (temp > max) max = temp;
                hm.put(s, temp);
            } else {
                hm.put(s, 1);
            }
        }
        ArrayList<String> ties = new ArrayList<>();
        final int maxf = max;
        return hm.entrySet()
                .stream()
                .filter(en -> en.getValue() == maxf)
                .map(en -> en.getKey())
                .sorted(Comparator.reverseOrder())
                .findFirst()
                .get();
    }
}
