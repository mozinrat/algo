package set;

import org.apache.commons.codec.language.Soundex;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Created by rohit on 5/17/17. */
public class ActorProblem {
  private static final double THRESHOLD = 0.85;

  private static final BiPredicate<Actor, Actor> isSame =
      (a1, a2) -> {
        float distance =
            StringUtils.getLevenshteinDistance(
                a1.getOrderInsensitiveName(), a2.getOrderInsensitiveName());
        int max =
            Math.max(a1.getOrderInsensitiveName().length(), a2.getOrderInsensitiveName().length());
        float normalizedDistance = 1 - distance / max;
        return normalizedDistance >= THRESHOLD
            || Soundex.US_ENGLISH
                .encode(a1.getOrderInsensitiveName())
                .equals(Soundex.US_ENGLISH.encode(a2.getOrderInsensitiveName()));
      };

  public static void main(String... arg) {
    UnionFind<Actor> disjointSet;
    try {
      disjointSet = new UnionFind<>(getActors().collect(Collectors.toSet()));
      getMovies()
          .forEach(
              movie ->
                  movie
                      .getActors()
                      .stream()
                      .map(Actor::new)
                      .filter(Objects::nonNull)
                      .forEach(
                          actor -> {
                            Actor tempActor = disjointSet.find(actor);
                            if (tempActor != null) {
                              tempActor.incrementFrequency();
                            } else {
                              disjointSet.addElement(actor);
                            }
                          }));
      List<Actor> allActors =
          disjointSet.getRankMap().keySet().stream().collect(Collectors.toList());

      // n2 operation
      for (int i = 0; i < allActors.size(); i++) {
        for (int j = 0; j < allActors.size(); j++) {
          if (i == j) continue;
          else {
            Actor t = allActors.get(i);
            Actor u = allActors.get(j);
            if (isSame.test(t, u)) {
              if (t.getFrequency() >= u.getFrequency()) {
                disjointSet.union(t, u);
              } else {
                disjointSet.union(u, t);
              }
            }
          }
        }
      }

      // assume from stream we got a new actor
      Actor newActor = new Actor("Aamir Khan");

      Actor actor = disjointSet.find(newActor);
      if (actor != null) {
        if (!actor.getOrderInsensitiveName().equalsIgnoreCase(newActor.getOrderInsensitiveName())) {
          Actor exsisting = getFromSet(disjointSet.getParentMap().keySet(), newActor);
          exsisting.incrementFrequency(newActor.getFrequency());
          if (exsisting.getFrequency() > actor.getFrequency()) {
              disjointSet.updateParent(exsisting,actor);
          }
        } else {
          actor.incrementFrequency();
          disjointSet.addElement(actor);
        }
      }
      System.out.println(actor.getFrequency());

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static Stream<Actor> getActors() throws IOException {
    return Files.lines(Paths.get("actors.csv"))
        .map(
            lineString -> {
              String[] temp = lineString.split(",");
              return new Actor(Long.parseLong(temp[0]), temp[1]);
            });
  }

  private static Stream<Movies> getMovies() throws IOException {
    return Files.lines(Paths.get("movies.csv"))
        .map(
            lineString -> {
              String[] temp = lineString.split(",");
              return new Movies(
                  Long.parseLong(temp[0]), temp[1], temp[2], temp.length == 4 ? temp[3] : null);
            });
  }

  private static Actor getFromSet(Set<Actor> set, Actor obj) {
    if (set.contains(obj)) {
      for (Actor o : set) {
        if (obj.equals(o)) return o;
      }
    }
    return null;
  }
}
