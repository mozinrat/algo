package set;

import java.util.Arrays;
import java.util.List;

/**
 * Created by rohit on 5/17/17.
 */
public class Movies {
    private long id;
    private String movieName;
    private List<String> actors;
    private List<String> staring;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public List<String> getStaring() {
        return staring;
    }

    public void setStaring(List<String> staring) {
        this.staring = staring;
    }

    public Movies(long id, String movieName, String actors, String staring) {
        this.id = id;
        this.movieName = movieName;
        this.actors = Arrays.asList(actors.split(":"));
        this.staring = staring.split(":").length>0 ? Arrays.asList(staring.split(":")) : null;
    }
}
