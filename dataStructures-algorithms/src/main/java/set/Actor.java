package set;

import javax.validation.constraints.NotNull;
import java.util.Map;

/** Created by rohit on 5/17/17. */
public class Actor {

  private long id;
  @NotNull private String firstName;
  private String lastName;
  private long frequency = 0;
  private Map<String, String> attributes;

  public Actor(@NotNull String name) {
    String[] temp = name.split(" ");
    this.firstName = temp[0];
    this.lastName = temp[1];
    frequency++;
  }

  public Actor(long id, @NotNull String name) {
    this.id = id;
    String[] temp = name.split(" ");
    this.firstName = temp[0];
    this.lastName = temp[1];
    frequency++;
  }

  public long getId() {
    return id;
  }

  public Map<String, String> getAttributes() {
    return attributes;
  }

  public void incrementFrequency() {
    frequency++;
  }
  public void incrementFrequency(long frequency) {
    this.frequency+=frequency;
  }

  public long getFrequency() {
    return frequency;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Actor actor2 = (Actor) o;
    String tempName = getOrderInsensitiveName();
    String anotherTempName = actor2.getOrderInsensitiveName();
    return tempName.equalsIgnoreCase(anotherTempName);
  }

  @Override
  public int hashCode() {
    return firstName.compareToIgnoreCase(lastName) < 0
        ? (firstName + lastName).toLowerCase().hashCode()
        : (lastName + firstName).toLowerCase().hashCode();
  }

  @Override
  public String toString() {
    return firstName + " " + lastName;
  }

  public String getOrderInsensitiveName() {
    return firstName.compareToIgnoreCase(lastName) < 0
        ? firstName + lastName
        : lastName + firstName;
  }

}
