package set;

import java.util.HashSet;

/**
 * Created by rohit on 6/9/17.
 */
public class DisjointSetTest {
    public static void main(String[] args) {
        UnionFind<Integer> uf = new UnionFind<>(new HashSet<>());

        uf.addElement(1);
        uf.addElement(2);
        uf.addElement(3);
        uf.addElement(4);
        uf.addElement(5);
        uf.addElement(6);
        uf.addElement(7);
        uf.addElement(8);
        uf.addElement(9);
        uf.addElement(10);

        System.out.println(uf);

        uf.union(1,6);
        uf.union(1,3);

        System.out.println(uf);

        uf.union(2,4);
        uf.union(2,5);

        uf.union(1,2);
        uf.union(2,7);
        System.out.println(uf);

    }
}
