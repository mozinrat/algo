//package riplling;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//
///**
// * Created by rohit on 6/8/17.
// */
//public class Knapsack {
//    public static void main(String[] args) {
//        Integer[] w = {1, 1, 2, 2, 2, 3, 3, 5};
//        Arrays.sort(w);
//        int total = 10;
//        ArrayList<Integer> collected = recursiveKnapSack(total, w.length, w);
//        System.out.println(collected);
//    }
//
//    private static ArrayList<Integer> recursiveKnapSack(int W, int n, Integer[] wt) {
//        DP[][] memo = new DP[n][n];
//        if (n == 0 || W <= 0)
//            return new ArrayList<>();
//
//        for (int i = n - 1; i >= 0; i--) {
//            if (wt[i] > W) {
//                continue;
//            }
//            for (int j = 0; j < n - 1; j++) {
//                if (!memo.containsKey(W)) {
//                    ArrayList<Integer> set1 = recursiveKnapSack(W - wt[n - 1], n - 1, wt);
//                    ArrayList<Integer> set2 = recursiveKnapSack(W, n - 1, wt);
//                    set1.add(wt[n - 1]);
//                    if (set1.size() >= set2.size()) {
//                        memo.put(W - wt[n - 1], set1);
//                        return set1;
//                    } else {
//                        memo.put(W, set2);
//                        return set2;
//                    }
//                }
//                return memo.get(0);
//            }
//
//
//        }
//    }
//}
//
//class DP{
//    int weight;
//    int size;
//
//    public DP(int weight, int size) {
//        this.weight = weight;
//        this.size = size;
//    }
//}