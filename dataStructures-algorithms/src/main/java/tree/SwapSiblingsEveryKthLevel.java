package tree;

import java.util.LinkedList;
import java.util.Queue;


/**
 * Given a binary tree, having N level and integer values as level, Swap sibling nodes of every L level where L>=1
 * Complete problem statement at:
 * http://www.geeksforgeeks.org/swap-nodes-binary-tree-every-kth-level/
 * This solutiin does inplace swapping
 */
public class SwapSiblingsEveryKthLevel {

    Queue<TreeNode> queue = new LinkedList<>();

    public void doBFS(Queue<TreeNode> queue, int levelBeingOperated, int k) {

        if (queue.isEmpty()) {
            return;
        }
        if (levelBeingOperated % k == 0) {
            for (TreeNode node : queue) {
                swapSibling(node);
            }
        }

        Queue<TreeNode> thisLevelQueue = new LinkedList<>();

        while (!queue.isEmpty()) {
            TreeNode polled = queue.poll();
            if (polled.left != null)
                thisLevelQueue.add(polled.left);
            if (polled.right != null)
                thisLevelQueue.add(polled.right);

        }
        doBFS(thisLevelQueue, levelBeingOperated + 1, k);
    }

    public void swapSibling(TreeNode node) {
        TreeNode temp = node.left;
        node.left = node.right;
        node.right = temp;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode root2 = new TreeNode(2);
        TreeNode root3 = new TreeNode(3);
        TreeNode root4 = new TreeNode(4);
        TreeNode root5 = new TreeNode(5);
        TreeNode root6 = new TreeNode(6);
        TreeNode root7 = new TreeNode(7);

        root.left = root2;
        root.right = root3;

        root2.left = root4;
        root2.right = root5;

        root3.left = root6;
        root3.right = root7;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        new SwapSiblingsEveryKthLevel().doBFS(queue, 1, 2);
        System.out.println("");
    }
}

