package tree;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ashish Gupta on 27/03/17.
 */
public class PrintMaxSumPath {

    public Result doMagic(TreeNode root) {

        if (root == null) {
            return null;
        }

        Result leftMagic = doMagic(root.left);
        Result rightMagic = doMagic(root.right);

        if (leftMagic == null && rightMagic == null) {
            return new Result(root.val, new ArrayList<>(Arrays.asList(root.val)));
        } else if (leftMagic == null) {
            return changeResult(rightMagic, root.val, root.val);
        } else if (rightMagic == null) {
            return changeResult(leftMagic, root.val, root.val);
        } else {
            Result max = leftMagic.SumTillNow > rightMagic.SumTillNow ? leftMagic : rightMagic;
            return changeResult(max, root.val, root.val);
        }

    }

    public Result changeResult(Result magic, int toAddSum, int toAddList) {
        magic.PathTillNow.add(toAddList);
        magic.SumTillNow += toAddSum;
        return magic;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(10);
        TreeNode root2 = new TreeNode(-2);
        TreeNode root3 = new TreeNode(6);
        TreeNode root4 = new TreeNode(8);
        TreeNode root5 = new TreeNode(-4);
        TreeNode root6 = new TreeNode(7);
        TreeNode root7 = new TreeNode(5);

        root.left = root2;
        root.right = root3;

        root2.left = root4;
        root2.right = root5;

        root3.left = root6;
        root3.right = root7;

        final Result result = new PrintMaxSumPath().doMagic(root);
        System.out.println(result);
    }


    static class Result {
        int SumTillNow;
        ArrayList<Integer> PathTillNow = new ArrayList<>();

        public Result(int sumTillNow, ArrayList<Integer> pathTillNow) {
            SumTillNow = sumTillNow;
            PathTillNow = pathTillNow;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Result{");
            sb.append("SumTillNow=").append(SumTillNow);
            sb.append(", PathTillNow=").append(PathTillNow);
            sb.append('}');
            return sb.toString();
        }
    }
}