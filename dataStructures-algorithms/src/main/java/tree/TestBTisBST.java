package tree;

/**
 * Created by rohit on 5/8/17.
 */
public class TestBTisBST {
    public static void main(String[] args) {
        tree.TreeNode node = new tree.TreeNode(10);
        node.left = new tree.TreeNode(6);

        node.left.left = new tree.TreeNode(5);
        node.left.right = new tree.TreeNode(20);
        node.left.right.left = new tree.TreeNode(7);
        node.left.right.right = new tree.TreeNode(22);

        node.right = new tree.TreeNode(16);
        node.right.left = new tree.TreeNode(12);
        node.right.right = new tree.TreeNode(17);

        testValidBinarySearchTree(node);
    }

    public static void testValidBinarySearchTree(tree.TreeNode node) {
        System.out.println(testBSTRange(node, new Range(Integer.MIN_VALUE, Integer.MAX_VALUE)));

    }

    private static boolean testBSTRange(tree.TreeNode node, Range range) {
        if (node == null) {
            return true;
        }

        if (node.val < range.max && node.val > range.min) {
            boolean leftTest = testBSTRange(node.left, new Range(range.min, node.val));
            boolean rightTest = testBSTRange(node.right, new Range(node.val, range.max));
            return leftTest && rightTest;
        }
        return false;
    }

    static class Range {
        int min;
        int max;

        public Range(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public int getMin() {
            return min;
        }

        public void setMin(int min) {
            this.min = min;
        }

        public int getMax() {
            return max;
        }

        public void setMax(int max) {
            this.max = max;
        }
    }
}
