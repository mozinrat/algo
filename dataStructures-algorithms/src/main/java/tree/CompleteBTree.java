package tree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by rohit on 4/27/17.
 */
public class CompleteBTree {
    public static void levelOrderTraversal(tree.TreeNode root, Queue<tree.TreeNode> queue) {
        if (root == null) {
            return;
        }
        queue.add(root);
        while (!queue.isEmpty()) {
            tree.TreeNode temp = queue.poll();
            System.out.print(temp.val + "\t");
            if (temp.left != null) {
                queue.add(temp.left);
            }
            if (temp.right != null) {
                queue.add(temp.right);
            }
        }
    }

    public static boolean testFullTree(tree.TreeNode root, Queue<tree.TreeNode> queue) {
        if (root == null) {
            return true;
        }
        boolean flag = false; // when a none full data is seen it become true
        queue.add(root);
        while (!queue.isEmpty()) {
            tree.TreeNode temp = queue.poll();
            System.out.print(temp.val + "\t");

            if (temp.left != null) {
                if (flag) return false;
                queue.add(temp.left);
                flag = true;
            }


            if (temp.right != null) {
                if (!flag) return false;
                queue.add(temp.right);
                flag = false;
            }
        }
        return true;
    }

    private static int getMaxPath(tree.TreeNode root, Res max) {
        if (root == null) {
            return 0;
        }
        int l = getMaxPath(root.left, max);
        int r = getMaxPath(root.right, max);

        // Max path for parent call of root. This path must
        // include at-most one child of root
        int max_single = Math.max(Math.max(l, r) + root.val, root.val);

        // Max Top represents the sum when the Node under
        // consideration is the root of the maxsum path and no
        // ancestors of root are there in max sum path
        int max_top = Math.max(max_single, l + r + root.val);

        // Store the Maximum Result.
        max.val = Math.max(max.val, max_top);

        return max_single;
    }

    static int minimumDepth(tree.TreeNode root) {
        // Corner case. Should never be hit unless the code is
        // called on root = NULL
        if (root == null)
            return 0;

        // Base case : Leaf Node. This accounts for height = 1.
        if (root.left == null && root.right == null)
            return 1;

        // If left subtree is NULL, recur for right subtree
        if (root.left == null)
            return minimumDepth(root.right) + 1;

        // If right subtree is NULL, recur for right subtree
        if (root.right == null)
            return minimumDepth(root.left) + 1;

        return Math.min(minimumDepth(root.left),
                minimumDepth(root.right)) + 1;
    }

    /**
     *                           1
     *                         /   \
     *                        2     3
     *                       / \   / \
     *                      4   5 6  7
     *                     /
     *                    8
     * */
    public static void main(String[] args) {
        tree.TreeNode node = new tree.TreeNode(1);
        node.left = new tree.TreeNode(2);
        node.right = new tree.TreeNode(3);

        node.left.left = new tree.TreeNode(4);
        node.left.right = new tree.TreeNode(5);

        node.right.left = new tree.TreeNode(6);
//        data.right.right = new TreeNode(7);

        node.left.left.left = new tree.TreeNode(8);

        Queue<tree.TreeNode> queue = new LinkedList<>();
//        System.out.println(testFullTree(data, queue));
//        System.out.println(getMaxPath(data,new Res(0)));
        System.out.println(minimumDepth(node));
    }

    static class Res {
        public int val;
    }
}
