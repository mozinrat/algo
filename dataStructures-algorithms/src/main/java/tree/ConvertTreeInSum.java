package tree;

/**
 * Convert a given tree to its Sum Tree
 * Given a Binary Tree where each node has positive and negative values. Convert this to a tree where each node contains the sum of the left and right sub trees in the original tree. The values of leaf nodes are changed to 0.
 *
 * For example, the following tree
 *        10
 *      /    \
 *    -2     6
 *   /  \   /  \
 * 8    -4 7    5
 *
 * should be changed to
 *
 *
 *        20(4-2+12+6)
 *      /      \
 *  4(8-4)      12(7+5)
 * /   \       /  \
 * 0    0     0    0
 */
public class ConvertTreeInSum {

    public int doMagic(TreeNode node) {
        if (node == null) {
            return 0;
        }
        int leftNodeOriginal = doMagic(node.left);
        int rightNodeOriginal  = doMagic(node.right);
        int leftSum = 0;
        int rightSum = 0;

        if (node.left != null){
             leftSum = node.left.val;
        }
        if (node.right != null){
            rightSum = node.right.val;
        }
        int oldValue = node.val;
        node.val = leftSum +  rightSum + leftNodeOriginal +  rightNodeOriginal;
        return oldValue ;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(10);
        TreeNode root2 = new TreeNode(-2);
        TreeNode root3 = new TreeNode(6);
        TreeNode root4 = new TreeNode(8);
        TreeNode root5 = new TreeNode(-4);
        TreeNode root6 = new TreeNode(7);
        TreeNode root7 = new TreeNode(5);

        root.left = root2;
        root.right = root3;

        root2.left = root4;
        root2.right = root5;

        root3.left = root6;
        root3.right = root7;
        new ConvertTreeInSum().doMagic(root);
    }
}
