package tree;

/**
 * Created by rohit on 5/5/17.
 */
public class LCA {
    static boolean v1 = false, v2 = false;

    public static void main(String[] args) {
        tree.TreeNode root = new tree.TreeNode(1);
        tree.TreeNode a = new tree.TreeNode(2);
        tree.TreeNode b = new tree.TreeNode(3);

        root.left = new tree.TreeNode(4);
        root.left.left = a;
        root.left.right = new tree.TreeNode(5);

        root.right = new tree.TreeNode(6);
        root.left.right.left = b;
        System.out.println(findLCA(root, a, b).val);
        System.out.println(v1 && v2);

    }

    public static tree.TreeNode findLCA(tree.TreeNode root, tree.TreeNode a, tree.TreeNode b) {
        if (root == null) {
            return root;
        }
        if (root.val == a.val)
        {
            v1 = true;
            return root;
        }
        if (root.val == b.val)
        {
            v2 = true;
            return root;
        }

        tree.TreeNode llca = findLCA(root.left, a, b);
        tree.TreeNode rlca = findLCA(root.right, a, b);
        if (llca != null && rlca != null) {
            return root;
        }
        return (llca != null) ? llca : rlca;

    }
}
