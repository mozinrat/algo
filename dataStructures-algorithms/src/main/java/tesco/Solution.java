package tesco;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by rohit on 5/29/17.
 */
public class Solution {

    public static void main(String[] args) {
        System.out.println(ft("1001"));
    }

    private static int ft(String input){
        Queue<TreeNode> queue = new ArrayDeque<>();
        TreeNode root = new TreeNode(input.charAt(0)-'0');
        queue.add(root);
        TreeNode current = null;
        int i=1;
        boolean left = true;
        while (!queue.isEmpty()){
            TreeNode node = new TreeNode(input.charAt(i)-'0');
            queue.add(node);
            if(left) {
                current = queue.poll();
                queue.poll();
                current.left = node;
                left = false;
            }else {
                current.right = node;
                left = true;
            }
        }

        return 0;
    }

    static class TreeNode {
        int node;
        TreeNode left;
        TreeNode right;

        public TreeNode(int node) {
            this.node = node;
        }
    }
}