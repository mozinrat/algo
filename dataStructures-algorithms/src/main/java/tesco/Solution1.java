package tesco;

public class Solution1 {

  public int collectMoney(int input1, String input2) {
    int[][] cost = parseCollectionMatrix(input2, input1);
    return maxCollection(cost, input1 - 1, input1 - 1);
  }

  private static int maxCollection(int cost[][], int m, int n) {
    int i, j;
    int tc[][] = new int[m + 1][n + 1];
    tc[0][0] = cost[0][0];

    /* Initialize first column of total cost(tc) array */
    for (i = 1; i <= m; i++) tc[i][0] = tc[i - 1][0] + cost[i][0];

    /* Initialize first row of tc array */
    for (j = 1; j <= n; j++) tc[0][j] = tc[0][j - 1] + cost[0][j];

    /* Construct rest of the tc array */
    for (i = 1; i <= m; i++)
      for (j = 1; j <= n; j++) {
        tc[i][j] = Math.max(tc[i - 1][j], tc[i][j - 1]) + cost[i][j];
      }

    return tc[m][n];
  }

  private static int[][] parseCollectionMatrix(String input2, int input1) {
    int[][] cost = new int[input1][input1];
    String[] temp = input2.split("\\),\\(");
    for (int i = 0; i < input1; i++) {
      String s = temp[i];
      s = s.replaceAll("\\(|\\)", "");
      String[] numbers = s.split(",");
      for (int j = 0; j < input1; j++) {
        cost[i][j] = Integer.parseInt(numbers[j]);
      }
    }
    return cost;
  }
}
