package tesco;

import java.util.LinkedList;
import java.util.Queue;

// Read only region start
public class UserMainCode {

    public int familyHierarchy(String input1) {
        // Read only region end
        Queue<TreeNode> queue = new LinkedList<>();

        if (input1 == null || input1.isEmpty()) {
            throw new UnsupportedOperationException("familyHierarchy(String input1)");
        }

        int index = 1;
        TreeNode root = new TreeNode(input1.charAt(0));
        queue.add(root);
        boolean isleft = true;

        while (!queue.isEmpty() && index < input1.length()) {
            TreeNode peeked = queue.peek();

            while (!queue.isEmpty() && peeked.val == '0') {
                peeked = queue.poll();
            }

            if (peeked.val == '0') {
                break;
            }

            char left;
            char right;
            if (isleft) {
                left = input1.charAt(index);
                peeked.left = new TreeNode(left);
                queue.add(peeked.left);
                isleft = false;
            } else {
                right = input1.charAt(index);
                peeked.right = new TreeNode(right);
                queue.add(peeked.right);
                queue.poll();
                isleft = true;
            }
            index++;
        }
        return maxDepth(root);
    }

    int maxDepth(TreeNode node) {
        if (node == null)
            return -1;
        else {
            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);
            return Math.max(++lDepth, ++rDepth);
        }
    }

    static class TreeNode {
        char val;
        TreeNode left;
        TreeNode right;

        public TreeNode(char val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        UserMainCode uc = new UserMainCode();
        System.out.println(uc.familyHierarchy("1001"));
    }
}