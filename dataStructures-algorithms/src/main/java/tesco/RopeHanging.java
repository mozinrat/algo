package tesco;

import java.util.Arrays;

/**
 * Created by rohit on 6/5/17.
 */
public class RopeHanging {

    public static void main(String[] args) {
        int ropeLength = 10;
        int[][] cloths = {{0, 2}, {1, 3}, {1, 2}, {0, 1}};
        long visible = findVisible(ropeLength, cloths);
        System.out.println(visible);
    }

    private static long findVisible(int ropeLength, int[][] cloths) {
        int[] rope = new int[ropeLength];
        for(int j=1;j<=cloths.length;j++){
            int[] temp = cloths[j-1];
            for(int i = temp[0];i<temp[1];i++){
                rope[i] = j;
            }
        }
        return Arrays.stream(rope).distinct().filter(i -> i>0).count();
    }
}
