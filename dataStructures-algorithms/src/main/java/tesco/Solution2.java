package tesco;

import java.util.HashMap;

/**
 * Created by rohit on 5/23/17.
 */
public class Solution2 {
    public static void main(String[] args) {
        System.out.println(new Solution2().convertBase("01234", "abcdef", "213213"));
    }

    public String convertBase(String input1, String input2, String input3) {
        if (input1 == null || input2 == null || input3 == null || input1.length() < 2 || input1.length() > 40 || input2.length() < 2 || input2.length() > 40)
            throw new UnsupportedOperationException("convertBase(String input1,String input2,String input3)");

        if (input1.equals(input2)) {
            return input3;
        }

        int oldBase = input1.length();
        int newBase = input2.length();

        char[] oldD = input1.toCharArray();
        char[] newD = input2.toCharArray();
        char[] st = input3.toCharArray();

        // o(n)
        HashMap<Character, Integer> sourceMap = new HashMap<>(oldBase);
        for (int i = 0; i < oldBase; i++) {
            sourceMap.put(oldD[i], i);
        }

        long sum = 0;
        // o(n)
        for (int i = 0; i < input3.length(); i++) {
            if (!sourceMap.containsKey(st[i])) {
                throw new UnsupportedOperationException("convertBase(String input1,String input2,String input3)");
            }
            sum = sum * oldBase + sourceMap.get(st[i]);
        }

        StringBuilder answer = new StringBuilder("");
        //convert sum to new oldBase
        do {
            int reminder = (int) sum % newBase;
            sum = sum / newBase;
            answer.append(newD[reminder]);
        } while (sum > 0);

        return answer.reverse().toString();
    }
}
