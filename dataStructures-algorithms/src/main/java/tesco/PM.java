package tesco;

/**
 * Created by rohit on 6/5/17.
 */
public class PM {
    public static void main(String[] args) {
        MyLinkedList myLinkedList = new MyLinkedList(9);
        myLinkedList.next = new MyLinkedList(9);
        myLinkedList.next.next = new MyLinkedList(9);
        myLinkedList.next.next.next = new MyLinkedList(9);
        myLinkedList = add(myLinkedList, 99991);
        System.out.println(myLinkedList.toString());
    }

    private static MyLinkedList add(MyLinkedList root, int i) {
        root = reverseLL(root);
        MyLinkedList temp = root;
        int carry = 0;
        do {
            carry = carry + temp.n + (i % 10) > 9 ? 1 : 0;
            temp.n = (carry + temp.n + (i % 10)) % 10;
            temp = temp.next;
            i = i / 10;
        } while ((temp != null));

        root = reverseLL(root);
        if (carry == 1) {
            MyLinkedList head = new MyLinkedList(1);
            head.next = root;
            return head;
        }
        return root;
    }

    static MyLinkedList reverseLL(MyLinkedList myLinkedList) {
        MyLinkedList prev = null;
        MyLinkedList current = myLinkedList;
        MyLinkedList next;

        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        myLinkedList = prev;
        return myLinkedList;
    }

    static class MyLinkedList {
        int n;
        MyLinkedList next;

        public MyLinkedList(int n) {
            this.n = n;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder(Integer.toString(n));
            MyLinkedList next1 = next;
            while (next1 != null) {
                sb.append(",").append(next1.n);
                next1 = next1.next;
            }
            return sb.toString();
        }
    }

}
