package speech;

import javax.sound.sampled.*;
import java.io.File;

/**
 * Created by rohit on 5/2/17.
 */
public class SpeechListener {
    private static final long RECORD_TIME = 6000;

    public static void main(String[] args) {
        AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,44100.0f, 16, 2,4, 44100.0f, true);
        TargetDataLine line = open(format);
        try {
            Thread.sleep(RECORD_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            line.stop();
            line.close();
        }
    }

    public static TargetDataLine open(AudioFormat format) {
        if (AudioSystem.isLineSupported(Port.Info.MICROPHONE)) {
            try (TargetDataLine line = AudioSystem.getTargetDataLine(format)) {
                line.open();
                line.start();
                System.out.println("Start capturing......");
                AudioInputStream ais = new AudioInputStream(line);
                System.out.println("Start recording...");
                AudioSystem.write(ais, AudioFileFormat.Type.WAVE, new File("abc.wav"));
                return line;
            } catch (Exception e) {
                throw new IllegalArgumentException("No microphone found or can't access");
            }
        } else {
            throw new IllegalArgumentException("No microphone found or can't access");
        }
    }
}
