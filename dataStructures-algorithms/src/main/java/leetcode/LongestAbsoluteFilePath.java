package leetcode;

import java.util.Stack;

/**
 * 388. Longest Absolute File Path
 * https://leetcode.com/problems/longest-absolute-file-path/#/description
 */
public class LongestAbsoluteFilePath {

    public static int getLevel(String str) {
        String[] chunks2 = str.split("\t");
        return chunks2.length - 1;
    }

    public boolean isFile(String str) {
        return str.contains(".");
    }

    public int lengthLongestPath1(String input) {
        Stack<Integer> stack = new Stack<>();
        stack.push(0); // "dummy" length
        int maxLen = 0;
        for (String s : input.split("\n")) {
            int lev = getLevel(s);
            while (lev + 1 < stack.size()) {
                stack.pop(); // find parent
            }
            int len = stack.peek() + s.length() - lev + 1; // remove "/t", add"/"
            stack.push(len);
            // check if it is file
            if (isFile(s))
                maxLen = Math.max(maxLen, len - 1);
        }
        return maxLen;
    }

    public static void main(String[] args) {
        //String str = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext";
        String str = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext";
        System.out.println(new LongestAbsoluteFilePath().lengthLongestPath1(str));
    }
}
