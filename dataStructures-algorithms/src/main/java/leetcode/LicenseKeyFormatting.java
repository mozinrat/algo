package leetcode;

/**
 * 482. License Key Formatting
 * https://leetcode.com/problems/license-key-formatting/#/description
 */

public class LicenseKeyFormatting {
    public String licenseKeyFormatting(String S, int K) {
        StringBuilder sb = new StringBuilder("");
        StringBuilder sbSmall = new StringBuilder("");

        for (int i = S.length() - 1; i >= 0; i--) {
            final char ch = S.charAt(i);
            if (ch != '-') {
                sbSmall.append(ch);
                if (sbSmall.length() == K) {
                    sb.append('-').append(sbSmall);
                    sbSmall = new StringBuilder();
                }
            }
        }
        if(sbSmall.length() > 0){
            sb.append('-').append(sbSmall);
        }
        if(sb.length() > 0 && sb.charAt(0) == '-'){
            sb.deleteCharAt(0);
        }

        return sb.reverse().toString().toUpperCase();
    }

    public static void main(String[] args) {
        System.out.println(new LicenseKeyFormatting().licenseKeyFormatting("", 2));
    }
}