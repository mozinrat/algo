package leetcode;

/**
 * Created by Ashish Gupta on 02/04/17.
 */
public class PoorPigs {
    public static int poorPigs(int buckets, int minutesToDie, int minutesToTest) {
        int base = minutesToTest / minutesToDie;
        return getMinBase(buckets, base + 1);

    }

    public static int getMinBase(int buckets, int base){
        int count = 0;
        while(buckets > 0){
            count++;
            buckets = buckets / base;
        }
        return count;
    }
    public static void main(String[] args) {
        System.out.println(poorPigs(1023,1,1));
    }
}
