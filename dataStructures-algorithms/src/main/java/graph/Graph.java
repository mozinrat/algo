package graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by rohit on 5/8/17.
 */
public class Graph {
    private int v;
    private boolean dag;
    private Map<Integer, List<Integer>> adjacencyList;

    public Graph(int number_of_vertices, boolean dag) {
        v = number_of_vertices;
        this.dag = dag;
        adjacencyList = new HashMap<>(number_of_vertices);
    }

    public void addEdge(int source, int destination) {
        if (adjacencyList.get(source) == null) {
            List<Integer> neighbours = new LinkedList<>();
            neighbours.add(destination);
            adjacencyList.put(source, neighbours);
        } else {
            List<Integer> neighbours = adjacencyList.get(source);
            neighbours.add(destination);
        }
        if (!dag) {
            if (adjacencyList.get(destination) == null) {
                List<Integer> neighbours = new LinkedList<>();
                neighbours.add(source);
                adjacencyList.put(destination, neighbours);
            } else {
                List<Integer> neighbours = adjacencyList.get(destination);
                neighbours.add(source);
            }
        }
    }

    public List<Integer> getNeighbours(int source) {
        return adjacencyList.get(source);
    }

    public int getVertices() {
        return v;
    }

    public Map<Integer, List<Integer>> getAdjacencyList() {
        return adjacencyList;
    }

}
