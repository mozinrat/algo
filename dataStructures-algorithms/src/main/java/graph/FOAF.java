package graph;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by rohit on 5/27/17.
 */
public class FOAF {
    public static void main(String[] args) {
        Graph g = new Graph(7, false);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(0, 3);

        g.addEdge(1, 4);
        g.addEdge(1, 5);

        g.addEdge(2, 3);

        g.addEdge(4, 5);
        g.addEdge(4, 6);

        g.addEdge(5, 6);
        g.addEdge(6, 1);

        /***
         *  0 - 1,2,3
         *  1 - 0,4,5,6
         *  2 - 0,3
         *  3 - 0,2
         *  4 - 1,5,6
         *  5 - 1,4,6
         *  6 - 4,5,7,8,1,3
         *  7 - 8
         *  8 - 7
         *
         * **/

        System.out.println(g);

        int maxKnow = 3;
        int minKnow = 3;

        Map<Integer, List<Integer>> adjacencyList = g.getAdjacencyList();
        Iterator<Map.Entry<Integer, List<Integer>>> iterator = adjacencyList.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, List<Integer>> next = iterator.next();
            if (next.getValue().size() < minKnow) {
                List<Integer> source = adjacencyList.get(next.getKey());
                iterator.remove();
                for (int i : source) {
                    adjacencyList.get(i).remove(new Integer(next.getKey()));
                }
            }

        }
    }
}
