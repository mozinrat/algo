package graph;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Given a snake and ladder board, find the minimum number of dice throws required to reach the destination or last cell from source or 1st cell.
 * Board used is http://d1hyf4ir1gqw6c.cloudfront.net//wp-content/uploads/snakesladders-300x249.jpg
 */
public class SnakeAndLadder {
    static class QueueEntry {
        int cellId;
        int distance;

        public QueueEntry(int cellId, int distance) {
            this.cellId = cellId;
            this.distance = distance;
        }
    }

     public int getMinDiceThrows(int move[], int N) {

        boolean[] visited = new boolean[N];
        Arrays.fill(visited, false);

        Queue<QueueEntry> queue = new LinkedList<>();
        // Mark the node 0 as visited and enqueue it.
        visited[0] = true;
        QueueEntry s = new QueueEntry(0, 0); // distance of 0't vertex is also 0
        queue.add(s); // Enqueue 0'th vertex
        QueueEntry polled = s;
        while (!queue.isEmpty()) {
            polled = queue.poll();
            if (polled.cellId == N - 1)
                return polled.distance;

            // Otherwise deQueue the front entry and enqueue its adjacent vertices (or cell numbers reachable through a dice throw)

            for (int j = polled.cellId + 1; j <= (polled.cellId + 6) && j < N; ++j) {

                QueueEntry next = new QueueEntry(j, polled.distance + 1);
                visited[j] = true;

                // Check if there a snake or ladder at 'j' then tail of snake or top of ladder become the adjacent of 'i'
                if (move[j] != -1)
                    next.cellId = move[j];

                queue.add(next);
            }

        }
        return polled.distance;
    }

    public static void main(String[] args) {
        // constructing the board
        int N = 30;
        int moves[] = new int[N];
        Arrays.fill(moves, -1);

        //Set Ladders
        moves[2] = 21;
        moves[4] = 7;
        moves[10] = 25;
        moves[19] = 28;

        //Set Snakes
        moves[26] = 0;
        moves[20] = 8;
        moves[16] = 3;
        moves[18] = 6;
        System.out.println(new SnakeAndLadder().getMinDiceThrows(moves, N));
    }
}


