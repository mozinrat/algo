package graph;

import java.util.List;

/**
 * Created by rohit on 5/8/17.
 */
public class DagCycle {

    public static void main(String[] args) {
        Graph g = new Graph(4,true);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(1, 3);
        g.addEdge(2, 3);
        g.addEdge(0, 3);
        detectCycle(g);
    }

    private static void detectCycle(Graph g) {
        System.out.println(isCyclic(g,0,new boolean[g.getVertices()],new boolean[g.getVertices()]));
    }

    private static boolean isCyclic(Graph g, int v, boolean[] visited, boolean[] recursionStack) {
        if (visited[v] == false) {
            visited[v] = true;
            recursionStack[v] = true;
            List<Integer> neighbours = g.getNeighbours(v);
            if(neighbours!=null) {
                for (int i : neighbours) {
                    if (!visited[i] && isCyclic(g, i, visited, recursionStack) || recursionStack[i]) return true;
                }
            }
        }
        recursionStack[v] = false;
        return false;
    }

}