package graph;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a NxM Matrix, print all ways to reach from source to destination
 * blockage means 0;
 */
public class MazeSolving {
    static int count = 0;

    public void countWays(int x, int y, int[][] input, int[][] visited, int ways) {

        //if reached destination, increase count
        if (x == input.length - 1 && y == input[0].length) {
            count++;
        } else if (isValidIndex(input, x, y) && visited[x][y] != 1 && !isBlocked(input, x, y)) {
            visited[x][y] = 1;

            //go down
            countWays(x + 1, y, input, visited, ways);
            //go up
            countWays(x - 1, y, input, visited, ways);
            //go right
            countWays(x, y + 1, input, visited, ways);
            //go left
            countWays(x, y - 1, input, visited, ways);

            visited[x][y] = 0;
        }
        return;
    }

    public void printWays(int x, int y, int[][] input, int[][] visited, List<Integer> pathTillNow) {

        //if reached destination, print the total path
        if (x == input.length - 1 && y == input[0].length-1) {
            pathTillNow.add(input[x][y]);
            System.out.println(pathTillNow);
            return;
        } else if(isValidIndex(input, x, y) && visited[x][y] != 1 && !isBlocked(input, x, y)) {
            visited[x][y] = 1;
            pathTillNow.add(input[x][y]);

            //go right
            printWays(x, y + 1, input, visited, new ArrayList<>(pathTillNow));
            //go down
            printWays(x + 1, y, input, visited, new ArrayList<>(pathTillNow));
            //go up
            printWays(x - 1, y, input, visited, new ArrayList<>(pathTillNow));
            //go left
            printWays(x, y - 1, input, visited, new ArrayList<>(pathTillNow));

            visited[x][y] = 0;
        }
    }

    public boolean isValidIndex(int[][] input, int x, int y) {
        if (x < 0 || y < 0 || x > input.length - 1 || y > input[0].length - 1)
            return false;
        return true;
    }

    public boolean isBlocked(int[][] input, int x, int y) {
        return input[x][y] == 0;
    }

    public static void main(String[] args) {
        int[][] input = new int[][]{
                {1,  2,  3,  4},
                {5,  6,  0,  8},
                {0,  10, 0, 12},
                {13, 14, 15, 16}
        };
        int[][] visited = new int[input.length][input[0].length];
        new MazeSolving().printWays(1,0,input, visited, new ArrayList<Integer>());
        //System.out.println(count);
    }

}
