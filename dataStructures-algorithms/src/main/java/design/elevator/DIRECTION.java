package design.elevator;

public enum DIRECTION {
    UP, DOWN, STAND, MAINT;

    public static DIRECTION reverse(DIRECTION d){
        switch (d){
            case UP:return DOWN;
            case DOWN:return UP;
            case STAND:return STAND;
            default:return MAINT;
        }
    }
}