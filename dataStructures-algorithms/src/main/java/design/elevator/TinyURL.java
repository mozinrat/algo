package design.elevator;

import java.util.Hashtable;

public class TinyURL {

    private static final String ALPHABET_MAP = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int BASE = ALPHABET_MAP.length();
    private static int index = 1;
    private Hashtable<Integer, String> database = new Hashtable<>();

    public String encode(String longUrl) {
        ++index;
        int dbId = index;
        database.put(dbId, longUrl);

        StringBuilder sb = new StringBuilder();
        while (dbId > 0) {
            sb.append(ALPHABET_MAP.charAt(dbId % BASE));
            dbId /= BASE;
        }

        return sb.reverse().toString();
    }

    public String decode(String shortUrl) {
        //convert ot dbId first
        int Num = 0;

        for (int i = 0, len = shortUrl.length(); i < len; i++) {
            Num = Num * BASE + ALPHABET_MAP.indexOf(shortUrl.charAt(i));
        }
        return database.get(Num);

    }

    public static void main(String[] args) {
        TinyURL c = new TinyURL() ;
        System.out.println(c.encode(""));
        System.out.println(c.decode("cLt"));
    }
}