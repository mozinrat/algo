package misc;

import java.util.Deque;
import java.util.Stack;

/**
 * Created by rohit on 3/31/17.
 */
public class LongestPath {
    public static void main(String[] args) {
        String path = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext";
        Stack<Fil> stack = new Stack<>();
        int level = 0;
        for (int start = 0, current = 0; current < path.length(); current++) {
            if (path.charAt(current) == '\t') {
                level++;
                start++;
            }
            if (path.charAt(current) == '\n' || current==path.length()-1) {
                stack.push(new Fil(current - start, level, path.substring(start, current)));
                start = current + 1;
                level = 0;
            }
        }
        System.out.println(stack.pop());
    }

    private static int computeMaxSoFar(Deque<Fil> stack, int level) {
        Fil temp = stack.poll();
        int max = temp.isFile ? temp.length : computeMaxSoFar(stack, level);
        return max;
    }

    static class Fil {
        int length;
        int level;
        String file;
        boolean isFile;

        public Fil(int length, int level, String file) {
            this.length = length;
            this.level = level;
            this.file = file;
            this.isFile = file.contains(".");
        }

        public void setLength(int length) {
            this.length = length;
        }
    }
}
