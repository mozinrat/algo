package misc;

import java.util.Hashtable;

/**
 * Window String
 * Given a string S and a string T, find the minimum window in S which will contain all the characters in T in linear time complexity.
 * Note that when the count of a character C in T is N, then the count of C in minimum window in Sshould be at least N.
 * Example :
 * S = "ADOBECODEBANC"
 * T = "ABC"
 * Minimum window is "BANC"
 *  
 * Note:
 * •	If there is no such window in S that covers all characters in T, return the empty string ''.
 * •	If there are multiple such windows, return the first occurring minimum window ( with minimum start index ).
 */


public class WindowString {
    Hashtable<Character, Integer> bigMap;
    Hashtable<Character, Integer> mapSmall;

    public String minWindow(String S, String T) {

        int start = 0;
        int end = T.length() - 1;

        bigMap = getCountMap(S);
        mapSmall = getCountMap(T);


        if (end < start) {
            return "";
        }

        return S.substring(start, end + 1);
    }

    public int getStart(String S) {
        int i;
        for (i = S.length() - 1; i >= 0; i--) {
            Character ch = S.charAt(i);
            if (!bigMap.containsKey(ch)) {
                return -1;
            } else {
                int count = bigMap.get(ch);
                if (!mapSmall.containsKey(ch)) {
                    continue;
                } else if (count > mapSmall.get(ch)) {
                    bigMap.put(ch, count - 1);
                } else {
                    return i;
                }
            }
        }
        return i;
    }

    public int getEnd(String S) {
        int i;
        for (i = 0; i < S.length() - 1; i++) {
            Character ch = S.charAt(i);
            if (!bigMap.containsKey(ch)) {
                return -1;
            } else {
                int count = bigMap.get(ch);
                if (!mapSmall.containsKey(ch)) {
                    continue;
                } else if (count > mapSmall.get(ch)) {
                    bigMap.put(ch, count - 1);
                } else {
                    return i;
                }
            }
        }
        return i;
    }


    public Hashtable<Character, Integer> getCountMap(String S) {
        Hashtable<Character, Integer> map = new Hashtable<>();
        if (S == null || S.isEmpty()) {
            return map;
        }
        for (Character ch : S.toCharArray()) {
            if (map.containsKey(ch)) {
                map.put(ch, map.get(ch) + 1);
            } else {
                map.put(ch, 1);
            }
        }
        return map;
    }

    public static void main(String[] args) {
        System.out.println(new WindowString().minWindow("ADOBECODEBANC", "BOD"));
    }
}
