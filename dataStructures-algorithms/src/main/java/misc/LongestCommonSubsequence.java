package misc;

/**
 * Created by rohit on 4/27/17.
 */
public class LongestCommonSubsequence {

    public static void main(String[] args) {
        final StringBuilder builder = new StringBuilder("abc");
        builder.append("def");
        System.out.println(builder.toString());

        String s1="aggtab";
        String s2="gxtxayb";
        findLcs(s1,s2);
    }

    private static void findLcs(String first, String second) {
        int[][] memoization = new int[first.length()+1][second.length()+1];

        for(int i=1;i<=first.length();i++){
            for(int j=1;j<=second.length();j++){
                if(first.charAt(i-1)==second.charAt(j-1)){
                    memoization[i][j]=memoization[i-1][j-1]+1;
                }else {
                    memoization[i][j]=Math.max(memoization[i-1][j],memoization[i][j-1]);
                }
            }

        }
        System.out.println(memoization[first.length()][second.length()]);
    }
}
