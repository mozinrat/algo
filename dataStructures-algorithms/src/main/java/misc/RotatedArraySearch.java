package misc;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Ashish Gupta on 01/04/17.
 */
public class RotatedArraySearch {

    public int search(ArrayList<Integer> input, int k, int start, int end) {

        if (end < start || input == null) {
            return -1;
        }
        int mid = (start + end) / 2;
        if (input.get(mid) == k)
            return mid;
        if (input.get(mid) < input.get(end)) {
            //ideal case in the later part
            if (input.get(mid) < k) {
                return search(input, k, mid + 1, end);
            } else {
                return search(input, k, start, mid - 1);
            }
        } else {
            //ideal case in the front part
            if (input.get(start) > k) {
                return search(input, k, mid + 1, end);
            } else {
                return search(input, k, start, mid - 1);
            }
        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> input = new ArrayList<>(Arrays.asList(2, 3, 4, 5, 1));
        System.out.println(new RotatedArraySearch().search(input, 3, 0, input.size() - 1));
    }
}
