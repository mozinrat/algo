package misc;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rohit on 4/3/17.
 */
public class Trie {
    public char data;
    public boolean isEnd;
    public Map<Character, Trie> children;

    public void setData(char data) {
        this.data = data;
    }

    public void addString(String s) {
        addString(s, 0);
    }

    private void addString(String s, int index) {
        if (index == s.length()) {
            isEnd = true;
        } else {
            char c = s.charAt(index);
            if (children == null) {
                children = new HashMap<>(26);
            }
            if (children.get(c) == null) {
                Trie child = new Trie();
                child.setData(c);
                child.addString(s, ++index);
                children.put(c, child);
            } else {
                children.get(c).addString(s, ++index);
            }
        }

    }
}
