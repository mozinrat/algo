package misc;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rohit on 4/3/17.
 */
public class BoggleDFS {
    static Set<String> dictionary = new HashSet<>();
    static Trie trie = new Trie();
    
    public static void main(String[] args) {
        char[][] boggle = {
                {'a', 'b', 'c'},
                {'b', 'e', 'e'},
                {'d', 's', 'k'}};

//        solution2(boggle);
    }

    public static void solution2(char[][] boggle){
        trie.addString("geeks");
        trie.addString("bed");
        trie.addString("sed");
        printWords2(boggle);
    }

    private static void printWords2(char[][] boggle) {
        for (int i = 0; i < boggle.length; i++) {
            for (int j = 0; j < boggle.length; j++) {
                if(trie.children.containsKey(boggle[i][j])){
                    boolean[][] visited = new boolean[boggle.length][boggle.length];
                    search(boggle,i,j,trie.children.get(boggle[i][j]),visited);
                }
            }
        }
    }

    private static void search(char[][] boggle, int i, int j, Trie trie,boolean[][] visited) {
        if(trie.isEnd){
            System.out.println("found word");
        }
        for (int ii = Math.max(0, i - 1); ii <= Math.min(boggle.length - 1, i + 1); ii++) {
            for (int jj = Math.max(0, j - 1); jj <= Math.min(boggle.length - 1, j + 1); jj++) {
                if (!visited[ii][jj]) {
                    search(boggle,ii,jj,trie.children.get(boggle[ii][jj]),visited);
                }
            }
        }
        visited[i][j] = false;
    }

    public static void solution1(char[][] boggle) {
        dictionary.add("geeks");
        dictionary.add("bed");
        dictionary.add("sed");
        printWords(boggle);
    }

    private static void printWords(char[][] boggle) {
        for (int i = 0; i < boggle.length; i++) {
            for (int j = 0; j < boggle.length; j++) {
                boolean[][] visited = new boolean[boggle.length][boggle.length];
                dfs(boggle, i, j, "", visited);
            }
        }
    }

    private static void dfs(char[][] boggle, int i, int j, String builder, boolean[][] visited) {
        String temp = builder+boggle[i][j];
        visited[i][j] = true;
        if (dictionary.contains(temp)) {
            System.out.println(temp);
        }
        for (int ii = Math.max(0, i - 1); ii <= Math.min(boggle.length - 1, i + 1); ii++) {
            for (int jj = Math.max(0, j - 1); jj <= Math.min(boggle.length - 1, j + 1); jj++) {
                if (!visited[ii][jj]) {
                    dfs(boggle, ii, jj, temp, visited);
                }
            }
        }

        visited[i][j] = false;
    }

}
