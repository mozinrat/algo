package misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.concurrent.ThreadLocalRandom.current;

/**
 * Created by rohit on 6/1/17.
 */
public class TicTacToe {
    public static void main(String[] args) {
        int n = 9;
        int[][] board = new int[n][n];
        while (!play(board, n)) {
            print(board);
            board = new int[n][n];
            try {
                Thread.sleep(1000);
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        print(board);
    }

    private static boolean play(int[][] board, int n) {
        int counter = 0;
        int[] p1T = new int[2 * n + 2];
        int[] p2T = new int[2 * n + 2];
        List<int[]> playerList = new ArrayList<>();
        playerList.add(p1T);
        playerList.add(p2T);
        while (counter < (n * n)) {
            int p = counter % 2 + 1;
            if (move(board, n, p, playerList.get(p - 1))) {
                System.out.println("winner is " + p);
                return true;
            }
            counter++;
        }
        return false;
    }

    private static boolean move(int[][] board, int n, int p1, int[] p2T) {
        int[] point = getPoint(board, n);
        board[point[0]][point[1]] = p1;
        if (point[0] == point[1]) {
            p2T[2 * n]++;
            if (p2T[2 * n] == n) return true;
        } else if (point[0] + point[1] == 2 * n + 1) {
            p2T[2 * n + 1]++;
            if (p2T[2 * n + 1] == n) return true;
        }
        p2T[point[0]]++;
        if (p2T[point[0]] == n) return true;
        p2T[n + point[1]]++;
        if (p2T[n + point[1]] == n) return true;
        return false;
    }

    private static int[] getPoint(int[][] board, int n) {
        int x = current().nextInt(0, 200 * n) % n;
        int y = current().nextInt(0, 200 * n) % n;
        if (board[x][y] != 0) {
            return getPoint(board, n);
        }
        return new int[]{x, y};
    }

    private static void print(int[][] in) {
        for (int[] row : in) {
            System.out.println(Arrays.toString(row));
        }
    }
}
