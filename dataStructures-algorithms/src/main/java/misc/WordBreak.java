package misc;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by rohit on 4/27/17.
 */
public class WordBreak {

    private static final Set<String> dictionary = new HashSet<>(Arrays.asList("mobile", "samsung", "sam", "sung",
            "man", "mango", "icecream", "and", "go", "i", "love", "ice", "cream"));

    public static void main(String[] args) {
//        System.out.println(Instant.ofEpochMilli(1488232800000l).atZone(ZoneId.of("Europe/Paris")));
        String in = "iloveicecreamandmango";
        wordbreakUtil(in, in.length(), "");
    }

    public static void wordbreakUtil(String str, int size, String result) {
        for (int i = 1; i <= size; i++) {
            String prefix = str.substring(0, i);
            if (dictionary.contains(prefix)) {
                if (i == size) {
                    result += prefix;
                    System.out.println(result);
                    return;
                }
                wordbreakUtil(str.substring(i, size), size - i, result + prefix + " ");
            }
        }

    }

    public static void wordBreakDP(String str,boolean[] visited){

    }
}
