package misc;

/**
 * Created by rohit on 3/31/17.
 */
public class LicenseFormattor {
    public static void main(String[] args) {
        String lk = "whjje-sss-23hdf-ds3";//23-hdf-ds3 // 23hd-fds3
        int k = 3;
        System.out.println(format(lk, k));
    }

    private static String format(String lk, int k) {
        char[] builder = new char[lk.length()]; //9
        int dashCount = k;
        for (int i = lk.length() - 1; i >= 0; --i,--dashCount) {
            if (dashCount == 0) {
                builder[i] = '-';
                dashCount=k+1;
                continue;
            }
            if (lk.charAt(i) == '-') {
                builder[i] = Character.toUpperCase(lk.charAt(i-1));
            } else {
                builder[i] = Character.toUpperCase(lk.charAt(i));
            }
        }

        return new String(builder);
    }
}
