package misc;

import java.util.Arrays;

/**
 * Created by rohit on 5/29/17.
 */
public class SumZ {

    public static void main(String[] args) {
        int[] in = new int[]{-1,3,4,9,-2,-5,1,0,2};
        int[][] dp = new int[in.length][in.length];
        int counter = 0;

        for (int i=0;i<in.length;i++){
            for (int j=0;j<in.length;j++){
                if(i!=j) {
                    dp[i][j] = dp[i-1][j] + in[i] + in[j];
                    if(dp[i][j]==0) counter++;
                }
            }
        }
        print(dp);
    }

    static void print(int[][] in){
        for (int[] row : in){
            System.out.println(Arrays.toString(row));
        }
    }
}
