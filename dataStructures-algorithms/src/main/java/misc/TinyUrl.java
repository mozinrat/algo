package misc;

import java.util.regex.Pattern;

/**
 * Created by rohit on 3/31/17.
 */
public class TinyUrl {
    public static final char[] all = "abcdefghizklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    public static final Pattern small = Pattern.compile("[a-z]");
    public static final Pattern big = Pattern.compile("[A-Z]");
    public static final Pattern num = Pattern.compile("[0-9]");


    public static void main(String[] args) {
        String url = "www.amazon.com/fsdhkjhkjh1kjhkj12h12jkhkajhdjadassdsa";
        int id = 125;
        System.out.println(encode(url, id));
        System.out.println(decode(encode(url, id)));
    }

    private static String encode(String url, int id) {
        int[] base62 = new int[6];
        //        5342 =
        int i = 5;
        while (id > 0) {
            int remainder = id % 62;
            base62[i] = remainder;
            id = id / 62;
            --i;
        }
        StringBuilder builder = new StringBuilder();
        for (int d : base62) {
            builder.append(all[d]);
        }
        return builder.toString();
    }

    private static int decode(String url) {
        int i = 0;
        int pow = 0;
        for (int j = 5; j >= 0; j--) {
            char c = url.charAt(j);
            if (small.matcher(Character.toString(c)).matches()) {
                pow = c - 97;
            } else if (big.matcher(Character.toString(c)).matches()) {
                pow = c - 65 + 26;
            } else if (num.matcher(Character.toString(c)).matches()) {
                pow = c - 48 + 52;
            }
            i += pow * Math.pow(62, 5 - j);
        }
        return i;
    }
}