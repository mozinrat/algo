package flipkart;

import java.util.*;

/**
 * Created by rohit on 6/12/17.
 */
public class AlienDictionary {
    public static void main(String[] args) {
        List<String> dict = Arrays.asList("ab", "bcd", "ce", "de");
        List<Character> sorted = findOrder(dict);
        System.out.println(sorted);
    }

    private static List<Character> findOrder(List<String> dict) {
        Graph<Character> graph = new Graph();
        String prev = dict.get(0);
        addG(graph, prev);
        for (int i = 1; i < dict.size(); i++) {
            String current = dict.get(i);
            addG(graph, current);
            for (int j = 0; j < Math.min(prev.length(), current.length()); j++) {
                if (prev.charAt(j) == current.charAt(j)) {
                    continue;
                } else {
                    graph.addEdge(current.charAt(j), prev.charAt(j));
                    break;
                }
            }
            prev = current;
        }

        return graph.topoLogicalSort();
    }

    private static void addG(Graph<Character> graph, String prev) {
        for (Character c : prev.toCharArray()) {
            graph.addVertex(c);
        }
    }
}

class Graph<V> {

    private Map<V, List<V>> edgeMap = new HashMap<>();

    public void addVertex(V v) {
        if (!edgeMap.containsKey(v)) {
            edgeMap.put(v, new LinkedList<>());
        }
    }

    public void addEdge(V from, V to) {
        List<V> relations = edgeMap.get(from);
        relations.add(to);
    }

    void topologicalSortUtil(V v, Set<V> visited, Stack<V> recurseStack) {
        visited.add(v);
        for (V i : edgeMap.get(v)) {
            if (!visited.contains(i))
                topologicalSortUtil(i, visited, recurseStack);
        }
        recurseStack.push(v);
    }

    public List<V> topoLogicalSort() {
        Set<V> visited = new HashSet<>();
        Stack<V> dfsStack = new Stack<>();
        for (V startNode : edgeMap.keySet()) {
            if (!visited.contains(startNode))
                topologicalSortUtil(startNode, visited, dfsStack);
        }
        List<V> orderedVertices = new LinkedList<>(dfsStack);
        return orderedVertices;
    }
}