package flipkart;

import java.util.Arrays;

/**
 * Created by rohit on 6/12/17.
 */
public class GoldMiner {

    public static void main(String[] args) {
        int[][] mine = {{1, 2, 3, 4},
                {2, 1, 3, 2},
                {3, 3, 1, 2}};
        System.out.println(findMaxGold(mine));
    }

    private static int findMaxGold(int[][] mine) {
        int max = Integer.MIN_VALUE;
        int dp[][] = new int[mine.length][mine[0].length];
        for (int i = 0; i < mine.length; i++) {
            int cost = dp(mine, dp, i, 0);
            if (cost > max) {
                max = cost;
            }
        }
        printMatrix(dp);
        return max;
    }

    private static int dp(int[][] mine, int[][] dp, int x, int y) {
        if (x >= mine.length || y >= mine[0].length || x < 0 || y < 0)
            return 0;
        if (dp[x][y] != 0) {
            return dp[x][y];
        }
        dp[x][y] = mine[x][y] + Math.max(dp(mine, dp, x - 1, y + 1), dp(mine, dp, x + 1, y + 1));
        return dp[x][y];
    }

    public static void printMatrix(int[][] matrix) {
        for (int[] row : matrix) {
            System.out.println(Arrays.toString(row));
        }
    }
}
