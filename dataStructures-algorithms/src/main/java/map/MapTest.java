package map;

import java.util.HashMap;
import java.util.stream.IntStream;

/**
 * Created by rohit on 5/9/17.
 */
public class MapTest {

    public static void main(String[] args) {
        System.out.println(IntStream.range(2,20).filter(i -> i>20).findAny().isPresent());
        HashMap<Integer, Integer> hi = new HashMap<>();
        int s[] = {1, 3, 0, 5, 8, 5};
        int f[] = {2, 4, 6, 7, 9, 9};
        for (int i = 0; i < s.length; i++) {
            if (hi.get(f[i]) == null || hi.get(f[i]) < s[i]) {
                hi.put(f[i], s[i]);
            }
        }
        hi.entrySet().forEach(en -> System.out.println(en.getKey() + "::" + en.getValue()));
    }

}
