package map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SplittableRandom;

/**
 * Created by rohit on 5/16/17.
 */
public class HMvLHMPerf {
    static final int max = 30_000_000;

    public static void main(String[] args) {
        Map<Integer, String> hm = new HashMap<>(max);
        SplittableRandom random = new SplittableRandom();

        long time = System.currentTimeMillis();
        random.ints(max).forEach(i -> hm.put(i, "tet"));
        System.out.println("time to insert = " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        hm.forEach((k, v) -> test(k));
        System.out.println("time to iterate = " + (System.currentTimeMillis() - time));

        Map<Integer, String> hm2 = new LinkedHashMap<>();

        time = System.currentTimeMillis();
        random.ints(max).forEach(i -> hm2.put(i, "tet"));
        System.out.println("time to insert = " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        hm2.forEach((k, v) -> test(k));
        System.out.println("time to iterate = " + (System.currentTimeMillis() - time));

    }

    private static void test(int k) {
        return;
    }
}
