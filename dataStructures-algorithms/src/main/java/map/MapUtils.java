package map;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by rohit on 6/14/17.
 */
public class MapUtils {

    public static <K, V> Map<K, V> mapOf(Map.Entry<K, V>... entries) {
        try (Stream<Map.Entry<K, V>> stream = Stream.of(entries)) {
            return stream.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }
    }

    public static <K, V> Map.Entry<K, V> en(K key, V value) {
        return new AbstractMap.SimpleEntry<>(key, value);
    }

    public static void main(String[] args) {
        System.out.println(mapOf(en(1,2),en(2,3),en(3,4)));
    }
}
