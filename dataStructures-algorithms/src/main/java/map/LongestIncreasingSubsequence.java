package map;

/**
 * Created by rohit on 5/9/17.
 */
public class LongestIncreasingSubsequence {

    static int maxLen = 1;

    public static void main(String[] args) {
        int arr[] = {10, 22, 9, 33, 21, 50, 41, 60};
        int n = arr.length;
       recursion_lis(arr, n);
        System.out.println(maxLen);

    }

    private static int recursion_lis(int[] arr, int n) {
        if (n == 1) return 1;
        int currLength = 1;
        for (int i = 0; i < n; i++) {
            int subLen = recursion_lis(arr, i);
            if((arr[i]<arr[n-1]) && currLength < (1+subLen)){
                currLength = 1+subLen;
            }
        }
        if(maxLen<currLength){
            maxLen=currLength;
        }
        return currLength;
    }
}
